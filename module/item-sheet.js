import {
  ItemMtA
} from "./item.js";
import * as customui from "./ui.js";
import * as templates from "./templates.js";

/**
 * Extend the basic ItemSheet with some very simple modifications
 */
export class MtAItemSheet extends ItemSheet {
  constructor(...args) {
    super(...args);
  }

  /**
   * Extend and override the default options used by the Simple Item Sheet
   * @returns {Object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["mta-sheet", "sheet", "item"],
      width: 630,
      tabs: [{
        navSelector: ".tabs",
        contentSelector: ".sheet-body",
        initial: "traits"
      }]
    });
  }

  /* -------------------------------------------- */

  /**
   * Return a dynamic reference to the HTML template path used to render this Item Sheet
   * @return {string}
   */
  get template() {
    const path = "systems/mta/templates/items";
    return `${path}/${this.item.data.type}.html`;
  }

  /* -------------------------------------------- */

  /**
   * Prepare data for rendering the Item sheet
   * The prepared data object contains both the actor data as well as additional sheet options
   */
  getData() {
    const data = super.getData();
    const itemData = data.data;
    data.config = CONFIG.MTA;

    const owner = this.actor;
    
    if (owner?.data.data.characterType === "Changeling") {
      data.isChangeling = true;
    }
    if (["container","cover"].includes(this.item.type)) {
      data.data.contents = data.data.contents || [];
      data.inventory = this._getContainerInventory(data);
    }
    if (this.item.type === "werewolf_rite" || this.item.type === "facet" || owner?.data.data.characterType === "Werewolf") {
      data.potentialAttributesName = "Werewolf Traits";
      data.potentialAttributes = [];
      data.potentialAttributes.push({entries: Object.entries(CONFIG.MTA.werewolf_renown), name: "werewolf_renown"});
      data.potentialAttributes.push({entries: Object.entries(CONFIG.MTA.werewolf_traits), name: "werewolf_traits"});
    }
    if (this.item.type === "discipline_power" || this.item.type === "devotion" || this.item.type === "rite" || owner?.data.data.characterType === "Vampire") {
      data.potentialAttributesName = "Vampire Traits";
      data.potentialAttributes = [];
      data.potentialAttributes.push({entries: Object.entries(CONFIG.MTA.disciplines_common), name: "disciplines_common"});
      data.potentialAttributes.push({entries: Object.entries(CONFIG.MTA.disciplines_unique), name: "disciplines_unique"});
      if(owner && owner.data.data.disciplines_own) data.potentialAttributes.push({entries: Object.entries(owner.data.data.disciplines_own).map(ele => [ele[0], ele[1].label]), name: "disciplines_own"});
      data.potentialAttributes.push({entries: Object.entries(CONFIG.MTA.vampire_traits), name: "vampire_traits"});

      data.disciplines = [];
      data.disciplines = data.disciplines.concat(Object.values(CONFIG.MTA.disciplines_common));
      data.disciplines = data.disciplines.concat(Object.values(CONFIG.MTA.disciplines_unique));
      if(owner && owner.data.data.disciplines_own) data.disciplines = data.disciplines.concat((Object.values(owner.data.data.disciplines_own).map(d => d.label)));
    }
    if (this.item.type === "embed" || this.item.type === "interlock" || this.item.type === "exploit" || this.item.type === "formAbility" || owner?.data.data.characterType === "Demon") {
      data.potentialAttributesName = "Demon Traits";
      data.potentialAttributes = [];
      data.potentialAttributes.push({entries: Object.entries(CONFIG.MTA.demon_traits), name: "demon_traits"});
    }
    if (this.item.type === "contract" || owner?.data.data.characterType === "Changeling") {
      data.potentialAttributesName = "Changeling Traits";
      data.potentialAttributes = [];
      data.potentialAttributes.push({entries: Object.entries(CONFIG.MTA.changeling_traits), name: "changeling_traits"});
    }
    if(this.item.type === "merit") {
      data.specialMerit = CONFIG.MTA.SPECIAL_MERITS.some(merit => merit.name === this.item.name);
    }
    data.item = itemData;
    data.data = itemData.data;

    console.trace(data);
    return data;
  }

  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   * @param html {HTML}   The prepared HTML object ready to be rendered into the DOM
   */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    if (this.item.type === "container" || this.item.type === "cover") this._registerContainerListeners(html);


    // Add effect
    html.find('.effectAdd').click(event => {
      const data = this.getData();
      let effectList = data.data.effects ? duplicate(data.data.effects) : [];
      effectList.push({name: "", value: 0});
    
      this.item.update({
        ["data.effects"]: effectList
      });
    });

    // Remove effect
    html.find('.effectRemove').click(event => {
      const data = this.getData();
      let effectList = data.data.effects ? duplicate(data.data.effects) : [];
      const index = event.currentTarget.dataset.index;
      effectList.splice(index, 1);
    
      this.item.update({
        ["data.effects"]: effectList
      });
    });

    // Add attribute to dicepool
    html.find('.dicePoolAdd').click(event => {
      const data = this.getData();
      let attributeList = data.data.dicePool?.attributes ? duplicate(data.data.dicePool.attributes) : [];
      attributeList.push("attributes_physical.strength");
    
      this.item.update({
        ["data.dicePool.attributes"]: attributeList
      });
    });

    // Remove attribute from dicepool
    html.find('.dicePoolRemove').click(event => {
      const data = this.getData();
      let attributeList = data.data.dicePool?.attributes ? duplicate(data.data.dicePool.attributes) : [];
      const index = event.currentTarget.dataset.index;
      attributeList.splice(index, 1);
    
      this.item.update({
        ["data.dicePool.attributes"]: attributeList
      });
    });

    //Custom select text boxes
    customui.registerCustomSelectBoxes(html, this);
  }

  /* -------------------------------------------- */
  /*                  CONTAINERS                  */
  /* -------------------------------------------- */


  _getContainerInventory(data) {
    let inventory;
    if(this.item.type === "cover") {
      inventory = {
        merit: {
          label: "Merits",
          items: [],
          dataset: ["MTA.Rating"]
        },
        condition: {
          label: "Conditions",
          items: [],
          dataset: ["MTA.Persistent"]
        },
        tilt: {
          label: "Tilts",
          items: [],
          dataset: ["MTA.Environmental"]
        }
      };
    }
    else { // TODO: This is never used
      inventory = {
        firearm: {
          label: "Firearm",
          items: [],
          dataset: ["Dmg.", "Range", "Cartridge", "Magazine", "Init.", "Size"]
        },
        melee: {
          label: "Melee",
          items: [],
          dataset: ["Damage", "Type", "Initiative", "Size"]
        },
        armor: {
          label: "Armor",
          items: [],
          dataset: ["Rating", "Defense", "Speed", "Coverage"]
        },
        equipment: {
          label: "Equipment",
          items: [],
          dataset: ["Dice bonus", "Durability", "Structure", "Size"]
        },
        ammo: {
          label: "Ammo",
          items: [],
          dataset: ["Cartridge", "Quantity"]
        },
        container: {
          label: "Containers",
          items: [],
          dataset: ["Durability", "Structure", "Size"]
        }
      };
    }
    

    data.data.data.contents.forEach(item => {
      if (inventory[item.type]) {
        if (!inventory[item.type].items) {
          inventory[item.type].items = [];
        }
        inventory[item.type].items.push(item);
      }
    });
    return inventory;
  }


  _registerContainerListeners(html) {
    //this.form.ondragover = ev => this._onDragOver(ev);
    this.form.ondrop = ev => this._onDrop(ev);

    html.find('.item-row').each((i, li) => {
      if (li.classList.contains("header")) return;
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", this._onDragItemStart.bind(this), false);
    });

    html.find('.cell.item-name span').click(event => this._onItemSummary(event));

    //document.addEventListener("dragend", this._onDragEnd.bind(this));

    // Delete Inventory Item
    html.find('.item-delete').click(event => {
      const data = this.getData();
      const index = Number(event.currentTarget.dataset.index);
      const type = event.currentTarget.dataset.type;
      console.log("Type",type)
      console.log("dat", data)
      let itemList = duplicate(data.data.contents);
      let indexToDelete = this._getItemIndex(index,type,data);      
      
      console.log("indexToDelete", indexToDelete)
      if(indexToDelete > -1) {
        itemList.splice(indexToDelete, 1);
        this.item.update({
          ["data.contents"]: itemList
        });
      }
    });
  }

  _getItemIndex(index,type,data) {
    let foundIndex = -1;
    if(this.item.type === "cover") {
      let item = data.inventory[type].items[index];
      foundIndex = data.data.contents.indexOf(item);
    }
    else {
      foundIndex = index;
    } 
    return foundIndex;
  }

  _onItemSummary(event) {
    event.preventDefault();
    const data = this.getData();
    let li = $(event.currentTarget).parents(".item-row");

    let index = this._getItemIndex(Number(li.data("index")),li.data("type"),data)
    let item = data.data.contents[index];

    let chatData = duplicate(item.data);
    chatData.description = TextEditor.enrichHTML(chatData.description, {
      secrets: this.owner
    });

    //let chatData = item.getChatData({secrets: this.owner});
    let tb = $(event.currentTarget).parents(".item-table");

    let colSpanMax = [...tb.get(0).rows[0].cells].reduce((a, v) => (v.colSpan) ? a + v.colSpan * 1 : a + 1, 0);

    // Toggle summary
    if (li.hasClass("expanded")) {
      let summary = li.next(".item-summary");
      summary.children().children("div").slideUp(200, () => summary.remove());
    } else {
      let tr = $(`<tr class="item-summary"> <td colspan="${colSpanMax}"> <div> ${chatData.description} </div> </td> </tr>`);
      //let props = $(`<div class="item-properties"></div>`);
      //chatData.properties.forEach(p => props.append(`<span class="tag">${p}</span>`));
      //div.append(props);
      let div = tr.children().children("div");
      div.hide();
      li.after(tr);
      div.slideDown(200);
    }
    li.toggleClass("expanded");
  }

  async _onDragItemStart(event) {
    //event.preventDefault();
    //event.stopPropagation();
    const data = this.getData();

    const index = this._getItemIndex(Number(event.currentTarget.dataset.index),event.currentTarget.dataset.type,data);
    let item = data.data.contents[index];
    item = duplicate(item);

    event.dataTransfer.setData("text/plain", JSON.stringify({
      type: "Item",
      data: item
    }));
  }


  async _onDrop(event) {
    //event.preventDefault();
    //event.stopPropagation();   

    // Try to extract the data
    let data;
    try {
      data = JSON.parse(event.dataTransfer.getData('text/plain'));
    } catch (err) {
      return false;
    }
    console.log("DATER", data)
    if (data.type === "Item") {
      const ownData = this.getData();
      const item = await Item.fromDropData(data);
      
      if (data.data.containerID === this.item.id) {
        return false;
      }
      const newItem = duplicate(item.data);
      let itemList = duplicate(ownData.data.contents);
      newItem._id = undefined;
      newItem.containerID = this.item.id;
      /* newItem.flags.mta = {
        containerID: this.item.id
      }; */
      console.log("New", newItem)
      
      itemList.push(newItem);
      console.log("List", itemList)

      let i = await this.item.update({
        ["data.contents"]: itemList
      });
      console.log("ITEM", i)
      return i;
    }

    return false;
  }

  /** @override */
  async _updateObject(event, formData) {
    // TODO: This can be removed once 0.7.x is release channel
    if (!formData.data) formData = expandObject(formData);
    if(formData.data?.dicePool?.attributes) {
      formData.data.dicePool.attributes = Object.values(formData.data.dicePool.attributes);
    }
    if(formData.data?.effects) {
      formData.data.effects = Object.values(formData.data.effects);
    }
    // Update the Item
    await super._updateObject(event, formData);
  }
}