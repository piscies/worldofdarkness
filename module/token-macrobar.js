

export class TokenHotBar extends Application {
    constructor(options) {
        super(options);
      }

    /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
	static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["worldbuilding", "dialogue", "mta-sheet"],
            template: "systems/mta/templates/other/tokenHotBar.html",
            popOut: false,
            minimisable: false,
            resisable: false
      });
    }

    getData() {
        const data = super.getData();
        if(this.tokens){
            this.macros = [];

            data.showNonWielded = game.user.data.flags?.mta?.tokenHotBar?.showNonWielded;
            data.showEquipment = game.user.data.flags?.mta?.tokenHotBar?.showEquipment;

            // Prepare generic macros
            this.macros.push({
                name: "Perception",
                img: "systems/mta/icons/gui/perception.svg",
                sheetMacro: true,
                callback: () => {
                    this.tokens.forEach(token => {
                        if(token.actor) token.actor.rollPerception(true, true, token.actor);
                    })
                 }
            });

            // Create name for macro bar
            if(this.tokens.length > 1){
                let names = this.tokens.map(token => token.name);
                data.characterName = names.join(", ");
            }
            let typeOrder = new Map ([
                ["firearm", 0], 
                ["melee", 1]
            ])
            if(data.showEquipment){
                typeOrder.set("armor", 2);
                typeOrder.set("ammo", 3);
                typeOrder.set("equipment", 4);
                typeOrder.set("container", 5);
            }

            // Only show item macros if only 1 token was selected
            if(this.tokens.length === 1){
                let token = this.tokens[0];
    
                data.characterName = token.name;
                if(token.actor){
                    if(token.actor.data.data.characterType === "Mage" || token.actor.data.data.characterType === "Proximi"){
                        this.macros.push({
                            name: "Improvised Spellcasting",
                            img: "systems/mta/icons/gui/macro-improvisedSpell.svg",
                            sheetMacro: true,
                            callback: () => {
                                token.actor.castSpell();
                            }
                        });
                    }

                    // Add equipped items and favourited abilities
                        let equipped = token.actor.data.items.filter(item => (typeOrder.has(item.data.type) && (data.showNonWielded || item.data.data.equipped)) || item.data.data.isFavorite)
                        equipped.forEach(item => {
                            let itemEntity = token.actor.items.get(item.id);
                            this.macros.push({
                                name: item.data.name,
                                img: item.data.img,
                                type: item.data.type,
                                notEquipped: typeOrder.has(item.data.type) && !item.data.data.equipped,
                                isFavorite: item.data.data.isFavorite,
                                callback: () => {
                                    if (item.data.type === "spell") return token.actor.castSpell(itemEntity);
                                    return itemEntity.roll();
                                }
                        })
                    });
                }
            }
            
            // Sort the macros. Sheet macros > favorited abilities > equipped items (sorted by typeOrder), then alphabetically (except sheet macros)
            this.macros.sort((a,b) => (typeOrder.has(a.type) ? typeOrder.get(a.type) : (a.sheetMacro ? -2 : -1)) - (typeOrder.has(b.type) ? typeOrder.get(b.type) : (b.sheetMacro ? -2 : -1)) || ((a.sheetMacro && b.sheetMacro) ? 0 : a.name.localeCompare(b.name) ));

            data.macros = this.macros;
        }
        return data;
    }

    activateListeners(html) {
        super.activateListeners(html);
        html.find('.macro').click(ev => {
            let index = event.currentTarget.closest(".macro").dataset.index;
            if(this.macros) this.macros[index].callback();
        });
        html.find('.showNonWielded').click(async ev => {
            let toggle = !game.user.data.flags?.mta?.tokenHotBar?.showNonWielded;
            let updateData = {
                'flags': {
                    'mta': {
                        tokenHotBar: {showNonWielded: toggle}
                    }
                 }
            };
            await game.user.update(updateData);
            this.render(true);
        });
        html.find('.showEquipment').click(async ev => {
            let toggle = !game.user.data.flags?.mta?.tokenHotBar?.showEquipment;
            let updateData = {
                'flags': {
                    'mta': {
                        tokenHotBar: {showEquipment: toggle}
                    }
                 }
            };
            await game.user.update(updateData);
            this.render(true);
        });
        html.find('.settings').click(ev => {
            let l = $('.settings-menu')
            $('.settings-menu').toggle();
        });
      }

      static tokenHotbarInit() {
        return new TokenHotBar();
      }

}




